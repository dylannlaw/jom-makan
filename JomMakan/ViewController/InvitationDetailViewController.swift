//
//  InvitationDetailViewController.swift
//  JomMakan
//
//  Created by Lay-Then on 13/08/2016.
//  Copyright © 2016 Dylan. All rights reserved.
//

import UIKit

class InvitationDetailViewController: UIViewController {
    
    @IBOutlet weak var restaurantLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var purposeLabel: UILabel!
    
    var invitation: Invitation!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        self.restaurantLabel.text = self.invitation.restaurant.name
        self.dateLabel.text = dateFormatter.stringFromDate(self.invitation.dateTime)
        self.purposeLabel.text = self.invitation.purpose
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
