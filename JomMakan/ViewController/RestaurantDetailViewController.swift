//
//  RestaurantDetailViewController.swift
//  JomMakan
//
//  Created by Goh Thian Hock on 21/07/2016.
//  Copyright © 2016 Dylan. All rights reserved.
//


import UIKit
import MapKit

class RestaurantsDetailViewController: UIViewController, MKMapViewDelegate {
    
//  let invitationSegueIdentifier = "invitationSegueIdentifier"
    let annotReuse: String = "STATION_IDENTIFIDER"
    let regionRadius: CLLocationDistance = 1000
    
    @IBOutlet weak var invitationTableView: UITableView!
    @IBOutlet weak var restaurantNameLabel: UILabel!
    @IBOutlet weak var restaurantMapView: MKMapView!
    
    var restaurant: Restaurant!
    var restaurantInvitation: [Invitation] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        loadRestaurantDetail()
       
    }
    
    func loadRestaurantDetail() -> Void {
         restaurantNameLabel.text = restaurant?.name
         loadRestaurantLocation()
         LoadRestaurantInvitation()
    }
    
    func loadRestaurantLocation() -> Void {
        if let location: CLLocation? = restaurant?.location as CLLocation? {
            if (location != nil) {
                centerMapOnLocation(location!)
                let annotation: RestaurantAnnotation = RestaurantAnnotation(name: restaurant!.name, latitude: location!.coordinate.latitude, longitude: location!.coordinate.longitude)
                let mkPin: MKPinAnnotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: annotReuse)
                restaurantMapView.addAnnotation(mkPin.annotation!)
            }
        }
    }
    
    func LoadRestaurantInvitation() -> Void  {
        restaurantInvitation.removeAll()
        for item in DataAccessController.sharedInstance.invitations {
            if item.restaurant.name == self.restaurant.name{
                restaurantInvitation.append(item)
            }
        }
        invitationTableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  regionRadius * 2.0, regionRadius * 2.0)
        restaurantMapView.setRegion(coordinateRegion, animated: true)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {        
        
    }

    @IBAction func showInvite(sender: AnyObject) {
        let restaurantStoryboard: UIStoryboard = UIStoryboard(name: "Restaurant", bundle: nil)
        
        let addInviteVC: AddInvitationViewController = restaurantStoryboard.instantiateViewControllerWithIdentifier("AddInvitationViewController") as! AddInvitationViewController
        addInviteVC.restaurant = self.restaurant
        
        addInviteVC.completeInvite = {(invitation: Invitation!) -> Void in
            
            DataAccessController.sharedInstance.invitations.append(invitation)
            DataAccessController.sharedInstance.saveInvitation(invitation)
            let invitationsCount: Int = DataAccessController.sharedInstance.invitations.count
            print("Total reservation \(invitationsCount)")
            for item in DataAccessController.sharedInstance.invitations {
                print("Invitation for \(item.purpose) is booked in \(item.restaurant.name) at \(item.dateTime)")
            }
                        
            self.dismissViewControllerAnimated(true, completion: nil)
            self.tabBarController?.selectedIndex = 1
            
            //let tabbedController = self.parentViewController as! UITabBarController
            //tabbedController.selectedIndex = 2
        }
        
        // Wrap inside navigation controller
        let navController: UINavigationController = UINavigationController(rootViewController: addInviteVC)
        
        // Show it sliding up to cover the screen
        self.presentViewController(navController, animated: true, completion: nil)
    }
}

extension RestaurantsDetailViewController: UITableViewDelegate{
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        
        return self.restaurantInvitation.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50
    }
}


extension RestaurantsDetailViewController: UITableViewDataSource {
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "Cell")
        
    
            cell.textLabel?.text = restaurantInvitation[indexPath.row].purpose
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print("User tapped on \(indexPath.row)")
        
    }
    
}



class RestaurantAnnotation: NSObject, MKAnnotation {
    var name: String?
    var latitude: Double
    var longitude:Double
    
    var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
    init(name: String, latitude: Double, longitude: Double) {
        self.name = name
        self.latitude = latitude
        self.longitude = longitude
    }
}