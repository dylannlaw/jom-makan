//
//  RestaurantsTableViewController.swift
//  JomMakan
//
//  Created by Dylan Law Wai Chun on 16/07/2016.
//  Copyright © 2016 Dylan. All rights reserved.
//

import UIKit
import MapKit



class RestaurantsTableViewController: UIViewController {
    let restaurantDetailSagueIdentifier = "restaurantDetailSagueIdentifier"
    var restaurants = [Restaurant]()
    var refreshControl: UIRefreshControl!

    @IBOutlet weak var restaurantTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.refreshControl = UIRefreshControl()
        self.restaurantTableView.addSubview(refreshControl)
        self.refreshControl.addTarget(self, action: #selector(RestaurantsTableViewController.refresh), forControlEvents: UIControlEvents.ValueChanged)
        
        self.refresh()
    }

    func refresh() {
        self.refreshControl.beginRefreshing()
        DataAccessController.sharedInstance.loadRestaurants { (restaurants) in
            // Still in background thread
            // Can do any processor heavy here... it'll slowly run in the BG
            self.restaurants = restaurants
            
            dispatch_async(dispatch_get_main_queue(), {
                self.refreshControl.endRefreshing()
                // Only this in main thread
                self.restaurantTableView.reloadData()
            })
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if  segue.identifier == restaurantDetailSagueIdentifier,
            let destination: RestaurantsDetailViewController = segue.destinationViewController as? RestaurantsDetailViewController,
            restaurantIndex = restaurantTableView.indexPathForSelectedRow?.row
        {
            destination.restaurant = restaurants[restaurantIndex]
        }
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension RestaurantsTableViewController: UITableViewDelegate{

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return restaurants.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 80
    }
}

extension RestaurantsTableViewController: UITableViewDataSource {
    

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "RestaurantListingTableViewCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! RestaurantTableViewCell
        
        // Fetches the appropriate meal for the data source layout.
        let restaurant = restaurants[indexPath.row]
        
        cell.restaurant = restaurant
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print("User tapped on \(indexPath.row)")
        
    }

}

class RestaurantTableViewCell: UITableViewCell {
    // MARK: Properties
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var photoImageView: UIImageView!
    var restaurant: Restaurant! {
        didSet {
           updateDisplay()
        }
    }
    
    func updateDisplay() {
        self.nameLabel.text = restaurant.name
        if restaurant.image != nil {
            self.photoImageView.image = restaurant.image
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}