//
//  LoginViewController.swift
//  JomMakan
//
//  Created by Dylan Law Wai Chun on 16/07/2016.
//  Copyright © 2016 Dylan. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var textFieldTopConstraint: NSLayoutConstraint!

    var customers: [Customer]!
    let loginSagueIdentifier = "loginSuccesSegueIdentifier"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        usernameTextField.delegate = self
        passwordTextField.delegate = self
        passwordTextField.secureTextEntry = true
        self.customers = DataAccessController.sharedInstance.customers
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func validateUser(userInputAccount: Customer, customers: [Customer]) -> Bool {
        if userInputAccount.loginEmail != "" || userInputAccount.password != "" {
            for userAccount in customers {
                if userInputAccount.loginEmail.uppercaseString == userAccount.loginEmail.uppercaseString {
                    if userInputAccount.password == userAccount.password {
                        DataAccessController.sharedInstance.currentLoginCustomer = userAccount;
                        return true
                    }
                }
            }
        }
        return false
    }
    
    @IBAction func clickLogin(sender: AnyObject) {
        let userInputAccount: Customer = Customer(loginEmail : usernameTextField.text!, password: passwordTextField.text!, name: "")
        
        if validateUser(userInputAccount, customers: customers) {
            print("Login Successful")
            performSegueWithIdentifier(loginSagueIdentifier, sender: nil)
        } else {
            print("Login Fail")
            self.showAlert("Error", message: "Invalid login", okButtonTitle: "Ok")
        }
    }
    
}

extension LoginViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == usernameTextField {
            passwordTextField.becomeFirstResponder()
            return true
            
        } else {
            passwordTextField.resignFirstResponder()
            return true
        }
    }
}
