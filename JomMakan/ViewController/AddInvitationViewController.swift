//
//  InvitationViewController.swift
//  JomMakan
//
//  Created by Dylan Law Wai Chun on 16/07/2016.
//  Copyright © 2016 Dylan. All rights reserved.
//

import UIKit

class AddInvitationViewController: UIViewController {
    
    var confirmButton: UIButton?
    @IBOutlet weak var invitationPurposeTextView: UITextView!
    @IBOutlet weak var eventDatePicker: UIDatePicker!
    
    var completeInvite: ((invitation: Invitation!)->Void)?
    
    var restaurant: Restaurant!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(AddInvitationViewController.handleTap(_:)))
        self.view.addGestureRecognizer(tap)

        // Do any additional setup after loading the view.
        
    }
    
    func handleTap(gestureRecognizer: UIGestureRecognizer) {
        UIView.animateWithDuration(0.5, animations: {
            self.invitationPurposeTextView.resignFirstResponder()
            self.view.layoutIfNeeded()
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancel(sender: AnyObject) {
        self.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func confirmInvitation(sender: AnyObject) {
        
        let reservationDate:NSDate = eventDatePicker.date
        //organizer: Customer, restaurant: Restaurant, purpose: String)
        let  invite:Invitation = Invitation(organizer: DataAccessController.sharedInstance.currentLoginCustomer!,  restaurant: restaurant, purpose:invitationPurposeTextView.text)
        invite.dateTime = reservationDate
        
        self.completeInvite?(invitation: invite)
    }
    
    override func viewWillAppear(animated: Bool) {

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
