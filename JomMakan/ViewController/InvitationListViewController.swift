//
//  InvitationViewController.swift
//  JomMakan
//
//  Created by Lay-Then on 30/07/2016.
//  Copyright © 2016 Dylan. All rights reserved.
//

import UIKit

class InvitationListViewController: UIViewController {
    @IBOutlet weak var invitationTableView: UITableView!
    
    var invitations: [Invitation] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        for invitation in DataAccessController.sharedInstance.invitations {
//            invitations.append(invitation)
//        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    
        invitations.removeAll()
        invitationTableView.reloadData()
        for invitation in DataAccessController.sharedInstance.invitations {
            invitations.append(invitation)
        }
        invitationTableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showInvitationDetail" {
            let destVC = segue.destinationViewController as! InvitationDetailViewController
            
            if let selectedIndex = self.invitationTableView.indexPathForSelectedRow {
                let invitation: Invitation = self.invitations[selectedIndex.row]
                destVC.invitation = invitation
            }
        }
    }
    
}

extension InvitationListViewController: UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return invitations.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: InvitationCell = self.invitationTableView.dequeueReusableCellWithIdentifier("InvitationCell", forIndexPath: indexPath) as! InvitationCell
        
        let invitation:Invitation =  self.invitations[indexPath.row]
        cell.invitation = invitation
        
        return cell
    }
}

extension InvitationListViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
}