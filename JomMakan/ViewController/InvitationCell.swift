//
//  InvitationCell.swift
//  JomMakan
//
//  Created by Lay-Then on 06/08/2016.
//  Copyright © 2016 Dylan. All rights reserved.
//

import UIKit

class InvitationCell: UITableViewCell {
    
    @IBOutlet weak var restaurantLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    var invitation: Invitation! {
        didSet {
            updateDisplay()
        }
    }
    
    func updateDisplay() {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        self.restaurantLabel.text = self.invitation.restaurant.name
        self.dateLabel.text = dateFormatter.stringFromDate(self.invitation.dateTime)
    }
}
