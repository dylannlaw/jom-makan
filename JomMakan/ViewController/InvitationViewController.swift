//
//  InvitationViewController.swift
//  JomMakan
//
//  Created by Dylan Law Wai Chun on 16/07/2016.
//  Copyright © 2016 Dylan. All rights reserved.
//

import UIKit

class InvitationViewController: UIViewController {
    
    @IBOutlet weak var invitationTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension InvitationViewController: UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "Cell")
        
        switch indexPath.row {
        case 0:
            cell.textLabel?.text = "Row 0"
        case 1:
            cell.textLabel?.text = "Row 1"
        case 2:
            cell.textLabel?.text = "Row 2"
        default:
            cell.textLabel?.text = "Unknown"
        }
        
        return cell
    }
}
