//
//  UIViewControllerAlert.swift
//  JomMakan
//
//  Created by Lay-Then on 13/08/2016.
//  Copyright © 2016 Dylan. All rights reserved.
//

import UIKit

extension UIViewController {
    func showAlert (title: String, message: String, okButtonTitle: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        
        let defaultAction = UIAlertAction(title: okButtonTitle, style: .Default, handler: nil)
        alertController.addAction(defaultAction)
        
        presentViewController(alertController, animated: true, completion: nil)
    }
    
}
