//
//  Invitation.swift
//  JomMakan
//
//  Created by Lay-Then on 21/07/2016.
//  Copyright © 2016 Dylan. All rights reserved.
//

import Foundation

class Invitation: NSObject, NSCoding {
    
    var organizer: Customer?
    var attendees: [Customer]? = []
    var restaurant: Restaurant
    var dateTime: NSDate
    var attendance: Bool?
    var purpose: String
    
    init (organizer: Customer, restaurant: Restaurant, purpose: String, dateTime: NSDate = NSDate()) {
        self.organizer = organizer
        self.restaurant = restaurant
        self.purpose = purpose
        self.dateTime = dateTime
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        let keyedArchiver: NSKeyedArchiver = aCoder as! NSKeyedArchiver
        keyedArchiver.encodeObject(organizer, forKey: "JMKInvitationOrganizer")
        keyedArchiver.encodeObject(attendees, forKey: "JMKInvitationAttendees")
        keyedArchiver.encodeObject(restaurant, forKey: "JMKInvitationRestaurant")
        keyedArchiver.encodeObject(dateTime, forKey: "JMKInvitationDate")
        keyedArchiver.encodeObject(attendance, forKey: "JMKInvitationAttendance")
        keyedArchiver.encodeObject(purpose, forKey: "JMKInvitationDescription")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let keyedUnarchiver: NSKeyedUnarchiver = aDecoder as! NSKeyedUnarchiver
        let organizer: Customer  = keyedUnarchiver.decodeObjectForKey("JMKRestaurantName") as! Customer
        let attendees: [Customer]  = keyedUnarchiver.decodeObjectForKey("JMKRestaurantName") as! [Customer]
        let restaurant: Restaurant  = keyedUnarchiver.decodeObjectForKey("JMKRestaurantName") as! Restaurant
        let dateTime: NSDate  = keyedUnarchiver.decodeObjectForKey("JMKRestaurantName") as! NSDate
        let attendance: Bool  = keyedUnarchiver.decodeObjectForKey("JMKRestaurantLocation") as! Bool
        let purpose: String  = keyedUnarchiver.decodeObjectForKey("JMKRestaurantLocation") as! String
        
        self.init(organizer: organizer, restaurant: restaurant, purpose: purpose)
        self.attendees = attendees
        self.dateTime = dateTime
        self.attendance = attendance
        
    }

    
}