//
//  DataAccessController.swift
//  JomMakan
//
//  Created by Goh Thian Hock on 26/07/2016.
//  Copyright © 2016 Dylan. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class DataAccessController {
    static let sharedInstance: DataAccessController = DataAccessController()
    
    var currentLoginCustomer: Customer?
    var restaurants: [Restaurant] = []
    var customers: [Customer] = []
    var invitations: [Invitation] = []
    
    let config: NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
    let session: NSURLSession

    let baseURL: String = "https://api.backendless.com/v1/data"

    private init() {
        self.config.HTTPAdditionalHeaders = ["application-id" : "8C457CBF-A9C1-FE17-FFD7-9A44424D1D00", "secret-key" : "FDD00661-3639-3C23-FF9E-A83B2DC31400", "Content-Type" : "application/json"]
        self.session  = NSURLSession(configuration: config)

        loadSampleCustomers();
    }
    
    func getURL(string: String, queryParameters: [String : String]) -> NSURL? {
        let components: NSURLComponents = NSURLComponents(string: baseURL + "/" + string)!

        var queryStrings: [String] = []
        for key in queryParameters.keys {
            queryStrings.append("\(key)=\(queryParameters[key]!)")
        }
        
        components.query = queryStrings.joinWithSeparator("&")
        
        return components.URL
    }
    
    func loadRestaurants(completion: ([Restaurant]) -> Void)  {
        let params: [String : String] = ["sortBy" : "Name"]
        let url: NSURL = getURL("Restaurant", queryParameters: params)!
        
        var restaurants: [Restaurant] = []
        let loadTask: NSURLSessionDataTask = session.dataTaskWithURL(url) {
            (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            
            if (data != nil) {
                if let json: [String : AnyObject] = try! NSJSONSerialization.JSONObjectWithData(data!, options:    NSJSONReadingOptions (rawValue: 0)) as? [String : AnyObject] {
                    
                    if let resultArray: [ [String : AnyObject] ] = json["data"] as? [ [String : AnyObject] ] {
                        for jsonData in resultArray {
                            if let restaurant: Restaurant = Restaurant(jsonData: jsonData) {
                                restaurants.append(restaurant)
                            }
                        }
                    }
                }
            }

            completion(restaurants)
        }
        loadTask.resume()
        
    }
    
    func saveInvitation(invitation: Invitation){
        
        let url: NSURL = NSURL(string: "https://api.backendless.com/v1/data/Invitation")!
        let urlRequest: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        urlRequest.HTTPMethod = "POST"
        urlRequest.addValue("application-id", forHTTPHeaderField: "8C457CBF-A9C1-FE17-FFD7-9A44424D1D00")
        urlRequest.addValue("secret-key", forHTTPHeaderField: "FDD00661-3639-3C23-FF9E-A83B2DC31400")
        urlRequest.addValue("Content-Type", forHTTPHeaderField: "application/json")
        urlRequest.addValue("application-type", forHTTPHeaderField: "REST")
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = .MediumStyle
        dateFormatter.timeStyle = .ShortStyle
        
        let jsonDict: [String : AnyObject] = ["purpose": invitation.purpose, "restaurant": invitation.restaurant.name ?? NSNull(), "organizer": invitation.organizer!.loginEmail ?? NSNull() ,
                                              "reservationDate" : dateFormatter.stringFromDate(invitation.dateTime) ?? NSNull()
        ]
        //, "reservationDate": invitation.dateTime ?? NSNull()
        let jsonData: NSData = try! NSJSONSerialization.dataWithJSONObject(jsonDict, options: NSJSONWritingOptions(rawValue: 0))
        
        urlRequest.HTTPBody = jsonData

        let session: NSURLSession = NSURLSession(configuration: config)
        
        let postTask: NSURLSessionDataTask = session.dataTaskWithRequest(urlRequest) { (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            // Check HTTP Code
            let httpCode: Int = (response as! NSHTTPURLResponse).statusCode
            print(httpCode)
        }
        
        postTask.resume()
    }
    
    func loadSampleCustomers() {
        let customerOne = Customer(loginEmail: "customer1@email.com", password: "abcd", name: "Customer A")
        let customerTwo = Customer(loginEmail: "customer2@email.com", password: "abcd", name: "Customer B")
        let customerThree = Customer(loginEmail: "customer3@email.com", password: "abcd", name: "Customer C")
        let customerFour = Customer(loginEmail: "customer4@email.com", password: "abcd", name: "Customer D")
        let customerFive = Customer(loginEmail: "customer5@email.com", password: "abcd", name: "Customer E")
        
        currentLoginCustomer = customerOne
        customers += [customerOne, customerTwo, customerThree, customerFour, customerFive]
        
    }
    
    func writeRestaurantsToFile() {
        let fileManager: NSFileManager = NSFileManager.defaultManager()
        let documentDirectory: NSURL = fileManager.URLsForDirectory(NSSearchPathDirectory.DocumentDirectory, inDomains: NSSearchPathDomainMask.UserDomainMask).first!
        let filePath: NSURL = documentDirectory.URLByAppendingPathComponent("restaurants.plist")
        
        var restaurantsData: [NSData] = []
        for restaurant in restaurants {
            let data: NSData = NSKeyedArchiver.archivedDataWithRootObject(restaurant)
            restaurantsData.append(data)
        }
        
        (restaurantsData as NSArray).writeToURL(filePath, atomically: true)
        
    }
    
    func readRestaurantsFromFile() {
         let fileManager: NSFileManager = NSFileManager.defaultManager()
         let documentDirectory: NSURL = fileManager.URLsForDirectory(NSSearchPathDirectory.DocumentDirectory, inDomains: NSSearchPathDomainMask.UserDomainMask).first!
        let filePath: NSURL = documentDirectory.URLByAppendingPathComponent("restaurants.plist")
        
        let restaurantsData: [NSData] = NSArray(contentsOfURL: filePath) as! [NSData]
        restaurants = []
        
        for data in restaurantsData {
            let restaurant: Restaurant = NSKeyedUnarchiver.unarchiveObjectWithData(data) as! Restaurant
            restaurants.append(restaurant)
        }
    }
    
    func writeCustomersToFile() {
        let fileManager: NSFileManager = NSFileManager.defaultManager()
        let documentDirectory: NSURL = fileManager.URLsForDirectory(NSSearchPathDirectory.DocumentDirectory, inDomains: NSSearchPathDomainMask.UserDomainMask).first!
        let filePath: NSURL = documentDirectory.URLByAppendingPathComponent("customers.plist")
        
        var customersData: [NSData] = []
        for customer in customers {
            let data: NSData = NSKeyedArchiver.archivedDataWithRootObject(customer)
            customersData.append(data)
        }
        
        (customersData as NSArray).writeToURL(filePath, atomically: true)
        
    }
    
    func readCustomersFromFile() {
        let fileManager: NSFileManager = NSFileManager.defaultManager()
        let documentDirectory: NSURL = fileManager.URLsForDirectory(NSSearchPathDirectory.DocumentDirectory, inDomains: NSSearchPathDomainMask.UserDomainMask).first!
        let filePath: NSURL = documentDirectory.URLByAppendingPathComponent("customers.plist")
        
        let customersData: [NSData] = NSArray(contentsOfURL: filePath) as! [NSData]
        customers = []
        
        for data in customersData {
            let customer: Customer = NSKeyedUnarchiver.unarchiveObjectWithData(data) as! Customer
            customers.append(customer)
        }
    }
    
    func writeInvitationsToFile() {
        let fileManager: NSFileManager = NSFileManager.defaultManager()
        let documentDirectory: NSURL = fileManager.URLsForDirectory(NSSearchPathDirectory.DocumentDirectory, inDomains: NSSearchPathDomainMask.UserDomainMask).first!
        let filePath: NSURL = documentDirectory.URLByAppendingPathComponent("invitations.plist")
        
        var invitationsData: [NSData] = []
        for invitation in invitations {
            let data: NSData = NSKeyedArchiver.archivedDataWithRootObject(invitation)
            invitationsData.append(data)
        }
        
        (invitationsData as NSArray).writeToURL(filePath, atomically: true)
        
    }
    
    func readInvitationsFromFile() {
        let fileManager: NSFileManager = NSFileManager.defaultManager()
        let documentDirectory: NSURL = fileManager.URLsForDirectory(NSSearchPathDirectory.DocumentDirectory, inDomains: NSSearchPathDomainMask.UserDomainMask).first!
        let filePath: NSURL = documentDirectory.URLByAppendingPathComponent("invitations.plist")
        
        let invitationsData: [NSData] = NSArray(contentsOfURL: filePath) as! [NSData]
        invitations = []
        
        for data in invitationsData {
            let invitation: Invitation = NSKeyedUnarchiver.unarchiveObjectWithData(data) as! Invitation
            invitations.append(invitation)
        }
    }
    
    
}