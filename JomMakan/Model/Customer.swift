//
//  Customer.swift
//  JomMakan
//
//  Created by Dylan Law Wai Chun on 17/07/2016.
//  Copyright © 2016 Dylan. All rights reserved.
//

import Foundation
import UIKit

class Customer: NSObject, NSCoding {
    var loginEmail: String
    var password: String
    var name: String
    var image: UIImage?
    var contactNumber: String?
    
    var invitation:[Invitation] = []
    
    init(loginEmail: String, password: String, name: String) {
        self.loginEmail = loginEmail
        self.password = password
        self.name = name
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        let keyedArchiver: NSKeyedArchiver = aCoder as! NSKeyedArchiver
        keyedArchiver.encodeObject(loginEmail, forKey: "JMKCustomerEmail")
        keyedArchiver.encodeObject(password, forKey: "JMKCustomerPassword")
        keyedArchiver.encodeObject(name, forKey: "JMKCustomerName")
        keyedArchiver.encodeObject(contactNumber, forKey: "JMKCustomerContact")
        if self.image != nil {
            let imageData: NSData? = UIImagePNGRepresentation(image!)
            keyedArchiver.encodeObject(imageData, forKey: "JMKCustomerImage")
        }
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let keyedUnarchiver: NSKeyedUnarchiver = aDecoder as! NSKeyedUnarchiver
        let email: String  = keyedUnarchiver.decodeObjectForKey("JMKCustomerEmail") as! String
        let password: String  = keyedUnarchiver.decodeObjectForKey("JMKCustomerPassword") as! String
        let name: String  = keyedUnarchiver.decodeObjectForKey("JMKCustomerName") as! String
        let contact: String  = keyedUnarchiver.decodeObjectForKey("JMKCustomerContact") as! String
        
        self.init(loginEmail: email, password: password, name:name)
        
        if let imageData = keyedUnarchiver.decodeObjectForKey("JMKCustomerImage") as? NSData {
            self.image = UIImage(data: imageData)
        }
        self.contactNumber = contact
        
    }
    
    func addInvitation() {
        
    }
}