//
//  Restaurant.swift
//  JomMakan
//
//  Created by Goh Thian Hock on 16/07/2016.
//  Copyright © 2016 Dylan. All rights reserved.
//

import Foundation
import MapKit

class Restaurant: NSObject, NSCoding {
    var id: String?
    var name: String
    var address: String?
    var contactNumber: String?
    var type: String?
    var location: CLLocation?
    var operatingHourStart: NSDate?
    var operatingHourEnd: NSDate?
    var image: UIImage?
    var email: String?
    
    init?(name:String) {
        self.name = name
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        let keyedArchiver: NSKeyedArchiver = aCoder as! NSKeyedArchiver
        keyedArchiver.encodeObject(name, forKey: "JMKRestaurantName")
        keyedArchiver.encodeObject(location, forKey: "JMKRestaurantLocation")
        if self.image != nil {
            let imageData: NSData? = UIImagePNGRepresentation(image!)
            keyedArchiver.encodeObject(imageData, forKey: "JMKRestaurantImage")
        }
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let keyedUnarchiver: NSKeyedUnarchiver = aDecoder as! NSKeyedUnarchiver
        let name: String  = keyedUnarchiver.decodeObjectForKey("JMKRestaurantName") as! String
        let location: CLLocation  = keyedUnarchiver.decodeObjectForKey("JMKRestaurantLocation") as! CLLocation
        self.init(name: name)
        if let imageData = keyedUnarchiver.decodeObjectForKey("JMKRestaurantImage") as? NSData {
            self.image = UIImage(data: imageData)
        }
        
        
        self.location = location
        
    }
    
    convenience init?(jsonData: [String : AnyObject]) {
        let name: String = jsonData["Name"] as! String
        
        self.init(name: name)

        if let latitude: Double = jsonData["Latitude"] as? Double {
            if let longitude: Double = jsonData["Longitude"] as? Double {
                self.location = CLLocation(latitude: latitude, longitude: longitude)
            }
        }
        
        if let imageURL: String = jsonData["Image"] as? String {
            if let url: NSURL = NSURL(string: imageURL) {
                if let imageData: NSData = NSData(contentsOfURL: url) {
                    self.image = UIImage(data: imageData)
                }
            }
        }

    }
}