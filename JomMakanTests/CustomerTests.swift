//
//  CustomerTests.swift
//  JomMakan
//
//  Created by Lay-Then on 23/07/2016.
//  Copyright © 2016 Dylan. All rights reserved.
//

import XCTest
@testable import JomMakan

class CustomerTests: XCTestCase {

//    var dummyOrganizer: UserAccount!
//    var dummyCustomer: Customer!
//    let attendees: [Customer] = [
//        Customer(name: "Lay Then", loginEmail: "laythen@gmail.com", userAccount: UserAccount(username: "laythen@gmail.com", password: "1234"))
//        ,
//        Customer(name: "Goh", loginEmail: "goh@gmail.com", userAccount: UserAccount(username: "goh@gmail.com", password: "1234"))
//    ]
    
    override func setUp() {
        super.setUp()
        
//        self.dummyOrganizer = UserAccount(username: "hello@gmail.com", password: "1234")
//        self.dummyCustomer = Customer(name: "Dummy", loginEmail: "hello@gmail.com", userAccount: self.dummyOrganizer)
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    func restaurantWithStartHour(startHour: Int, endHour: Int) -> Restaurant {
        let myRestaurant: Restaurant! = Restaurant(name: "Starbucks")
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone(forSecondsFromGMT: 8 * 60 * 60)
        
        myRestaurant.operatingHourStart = dateFormatter.dateFromString("\(startHour):00:00")
        myRestaurant.operatingHourEnd = dateFormatter.dateFromString("\(endHour):00:00")
        
        return myRestaurant
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
//    func addInvitation(myAccount: UserAccount, myRestaurant: Restaurant, myInvitation: Invitation) -> Bool {
//        let startTimeComponents: NSDateComponents = NSCalendar.currentCalendar().components([NSCalendarUnit.Hour, NSCalendarUnit.Minute], fromDate: myRestaurant.operatingHourStart!)
//        let endTimeComponents: NSDateComponents = NSCalendar.currentCalendar().components([NSCalendarUnit.Hour, NSCalendarUnit.Minute], fromDate: myRestaurant.operatingHourEnd!)
//        
//        let minHour: Int = min(startTimeComponents.hour, endTimeComponents.hour)
//        let maxHour: Int = max(startTimeComponents.hour, endTimeComponents.hour)
//        
//        let invitationComponents: NSDateComponents = NSCalendar.currentCalendar().components([NSCalendarUnit.Hour, NSCalendarUnit.Minute], fromDate: myInvitation.dateTime!)
//        
//        if endTimeComponents.hour < startTimeComponents.hour {
//            if (invitationComponents.hour >= 0 && invitationComponents.hour <= minHour) || (invitationComponents.hour >= maxHour && invitationComponents.hour < NSCalendar.currentCalendar().maximumRangeOfUnit(NSCalendarUnit.Hour).length) {
//                return true
//            }
//        } else {
//            return invitationComponents.hour >= minHour && invitationComponents.hour <= maxHour
//        }
//        
//        return false
//    }
//    
//    func testAddInvitationToDayRestaurant() {
//        
//        let dayRestaurant: Restaurant = self.restaurantWithStartHour(8, endHour: 17)
//        
//        let myInvitation: Invitation = Invitation(organizer: self.dummyCustomer, attendees: self.attendees, restaurant: dayRestaurant, attendance: true)
//        myInvitation.dateTime = NSCalendar.currentCalendar().dateBySettingHour(21, minute: 0, second: 0, ofDate: NSDate(), options: NSCalendarOptions.MatchStrictly)!
//        XCTAssertFalse(addInvitation(self.dummyOrganizer, myRestaurant: dayRestaurant, myInvitation: myInvitation))
//        
//        let myInvitation2: Invitation = Invitation(organizer: self.dummyCustomer, attendees: self.attendees, restaurant: dayRestaurant, attendance: true)
//        myInvitation2.dateTime = NSCalendar.currentCalendar().dateBySettingHour(8, minute: 0, second: 0, ofDate: NSDate(), options: NSCalendarOptions.MatchStrictly)!
//        XCTAssertTrue(addInvitation(self.dummyOrganizer, myRestaurant: dayRestaurant, myInvitation: myInvitation2))
//        
//        let myInvitation3: Invitation = Invitation(organizer: self.dummyCustomer, attendees: self.attendees, restaurant: dayRestaurant, attendance: true)
//        myInvitation3.dateTime = NSCalendar.currentCalendar().dateBySettingHour(19, minute: 0, second: 0, ofDate: NSDate(), options: NSCalendarOptions.MatchStrictly)!
//        XCTAssertFalse(addInvitation(self.dummyOrganizer, myRestaurant: dayRestaurant, myInvitation: myInvitation3))
//        
//        let myInvitation4: Invitation = Invitation(organizer: self.dummyCustomer, attendees: self.attendees, restaurant: dayRestaurant, attendance: true)
//        myInvitation4.dateTime = NSCalendar.currentCalendar().dateBySettingHour(11, minute: 0, second: 0, ofDate: NSDate(), options: NSCalendarOptions.MatchStrictly)!
//        XCTAssertTrue(addInvitation(self.dummyOrganizer, myRestaurant: dayRestaurant, myInvitation: myInvitation4))
//    }

}
