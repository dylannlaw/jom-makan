//
//  JomMakanAPITests.swift
//  JomMakan
//
//  Created by Dylan Law Wai Chun on 06/08/2016.
//  Copyright © 2016 Dylan. All rights reserved.
//

import XCTest
@testable import JomMakan

class JomMakanAPITests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testLoadRestaurants() {
        let expectation = self.expectationWithDescription("Load Restaurants")
        
        let url: NSURL = NSURL(string: "https://api.backendless.com/v1/data/Restaurant")!
        let config: NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        
        config.HTTPAdditionalHeaders =
            ["application-id" : "8C457CBF-A9C1-FE17-FFD7-9A44424D1D00",
             "secret-key" : "FDD00661-3639-3C23-FF9E-A83B2DC31400",
             "Content-Type" : "application/json"]
            
        let session: NSURLSession = NSURLSession(configuration: config)
            
        var restaurants: [Restaurant] = []
        
        let loadTask: NSURLSessionDataTask = session.dataTaskWithURL(url) { (data: NSData?, response: NSURLResponse?, error: NSError?) in
            
            if (data != nil) {
                if let json: [String : AnyObject] = try! NSJSONSerialization.JSONObjectWithData(data!, options:    NSJSONReadingOptions (rawValue: 0)) as? [String : AnyObject] {
                    
                    if let resultArray: [ [String : AnyObject] ] = json["data"] as? [ [String : AnyObject] ] {
                        for jsonData in resultArray {
                            let name: String = jsonData["Name"] as! String
                            NSLog(name)
                            
                            let restaurant: Restaurant = Restaurant(name: name)!
                            
                            restaurants.append(restaurant)
                            
                            
                        }
                        XCTAssert(restaurants.count > 0 , "Should have restaurants")
                    }
                }
            }
            expectation.fulfill()
        }
        
        
        
        loadTask.resume()
        
        self.waitForExpectationsWithTimeout(20, handler: nil)
        

    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }

}
