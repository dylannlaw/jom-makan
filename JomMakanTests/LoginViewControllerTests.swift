//
//  LoginViewControllerTests.swift
//  JomMakan
//
//  Created by Dylan Law Wai Chun on 21/07/2016.
//  Copyright © 2016 Dylan. All rights reserved.
//

import XCTest
@testable import JomMakan

class LoginViewControllerTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testLogin() {
        let loginVC: LoginViewController = LoginViewController()

        let userCorrectInputAccount: Customer = Customer(loginEmail: "ABC@GMaiL.com", password: "1@3$5^7*9)")
        let userWrongUsernameAccount: Customer = Customer(loginEmail: "DEF@GMaiL.com", password: "1@3$5^7*9)")
        let userWrongPasswordAccount: Customer = Customer(loginEmail: "ABC@GMaiL.com", password: "!2#4%6&8(0")
        
        let dummyAccount1: Customer = Customer(loginEmail: "a@b.com", password: "1234")
        let dummyAccount2: Customer = Customer(loginEmail: "c@d.com", password: "4567")
        let dummyAccount3: Customer = Customer(loginEmail: "abc@gmail.com", password: "1@3$5^7*9)")
        let customers: [Customer] = [dummyAccount1,  dummyAccount2, dummyAccount3]
        
        XCTAssert(loginVC.validateUser(userCorrectInputAccount, customers: customers), "User should be able to login with correct password")
        
        XCTAssert(loginVC.validateUser(userWrongUsernameAccount, customers: customers) == false, "User should not be able to login with wrong username")
        
        XCTAssert(loginVC.validateUser(userWrongPasswordAccount, customers: customers) == false, "User should not be able to login with wrong password")
    }
    

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }

}
